@extends('layouts.layout')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Data Pertanyaan</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List Pertanyaan</h3><br>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Tanggal Dibuat</th>
                                <th>Tanggal Diperbaharui</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key =>$row)
                            <tr>
                                <td>{{ $row->judul }}</td>
                                <td>{{ $row->isi }}</td>
                                <td>{{ $row->tanggal_dibuat }}</td>
                                <td>{{ $row->tanggal_diperbaharui }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Tanggal Dibuat</th>
                                <th>Tanggal Diperbaharui</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>

            <!-- /.card -->
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@push('scripts')
<!-- DataTables -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });

</script>
@endpush
