<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table='pertanyaan';
    protected $primaryKey='id';
    public $incrementing =true;
    public $timestamps=true; 
    const CREATED_AT = 'tanggal_dibuat';
    const UPDATED_AT = 'tanggal_diperbaharui'; 
    protected $fillabe = ['id','judul','isi','tanggal_dibuat','tanggal_diperbaharui','jawaban_tepat_id','profil_id'];
    protected $guarded = [];  

    //Setiap pertanyaan akan mempunyai 1 jawaban yang terpilih atau jawaban yang paling tepat. Buatlah relasi one to one antara model Pertanyaan dengan model Jawaban tersebut.
    public function jawaban()
    {
        return $this->hasOne('App\Jawaban','id','jawaban_tepat_id');
    }

    //User punya banyak Pertanyaan, 1 Pertanyaan kepunyaan 1 User.
    public function user(){
        return $this->belongsTo('App\User'); 
    }

    //1 Pertanyaan punya banyak Jawaban. 1 Jawaban kepunyaan 1 Pertanyaan
    public function jawaban2()
    {
        return $this->hasMany('App\Jawaban');
    }

    //Setiap satu Pertanyaan dapat diberikan banyak Tag di dalamnya. dan setiap Tag dapat terhubung ke banyak Pertanyaan. Buatlah relasi many to many untuk model Pertanyaan dan Tag!
    public function tag()
    {
        return $this->belongsToMany('App\Tag', 'katalog_pertanyaan', 'pertanyaan_id','tag_id');
    }  
}
