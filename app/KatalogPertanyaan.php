<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KatalogPertanyaan extends Model
{
    protected $table='katalog_pertanyaan';
    protected $primaryKey='id';
    public $incrementing =true;
    public $timestamps=false; 
    protected $fillabe = ['id','pertanyaan_id','tag_id'];
    protected $guarded = [];  
 
 }
