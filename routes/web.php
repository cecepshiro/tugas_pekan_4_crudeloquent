<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Pertanyaan Controller
Route::resource('pertanyaan', 'PertanyaanController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('coba/jawaban', 'PertanyaanController@jawaban');
Route::get('coba/pertanyaan', 'PertanyaanController@pertanyaan');
Route::get('coba/pertanyaan/{id}/detailJawaban', 'PertanyaanController@detailJawaban');
Route::get('coba/katalogpertanyaan/{id}/tag', 'PertanyaanController@katalogpertanyaan');
Route::get('coba/tagpertanyaan/{id}/tag', 'PertanyaanController@tagpertanyaan');