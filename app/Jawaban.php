<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table='jawaban';
    protected $primaryKey='id';
    public $incrementing =true;
    public $timestamps=true; 
    const CREATED_AT = 'tanggal_dibuat';
    const UPDATED_AT = 'tanggal_diperbaharui'; 
    protected $fillabe = ['id','isi','tanggal_dibuat','tanggal_diperbaharui','pertanyaan_id','profil_id'];
    protected $guarded = [];  

    public function pertanyaan(){
        return $this->belongsTo('App\Pertanyaan');
    }
}
