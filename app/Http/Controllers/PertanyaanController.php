<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;
use App\User;
use App\Tag;

class PertanyaanController extends Controller
{

    public function __construct(){
        // $this->middleware('auth')->except('index');
        $this->middleware('auth')->only(['create','store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pertanyaan::all();
        return view('pertanyaan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pertanyaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'isi' => 'required'
    	]);
 
        Pertanyaan::create([
    		'judul' => $request->judul,
    		'isi' => $request->isi
        ]);
        
        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pertanyaan::find($id);
        return view('pertanyaan.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pertanyaan::find($id);
        return view('pertanyaan.edit',  compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'isi' => 'required'
        ]);
        $data = Pertanyaan::find($id);
        $data->judul = $request->judul;
        $data->isi = $request->isi;
        $data->update();
        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Pertanyaan::find($id);
        $post->delete();
        return redirect('/pertanyaan');
    }

    public function jawaban()
    {
        $data = Pertanyaan::all();
        return view('pertanyaan.jawaban', compact('data'));
    }

    public function detailJawaban($id)
    {
        $data = Pertanyaan::find($id);
        return view('pertanyaan.detailjawaban', compact('data'));
    }

    public function pertanyaan()
    {
        $data = User::all();
        return view('pertanyaan.pertanyaan', compact('data'));
    }

    public function katalogpertanyaan($id)
    {
        $data = Pertanyaan::find($id);
        return view('pertanyaan.katalogpertanyaan', compact('data'));
    }

    public function tagpertanyaan($id){
        $data = Tag::find($id)->pertanyaan;
        return view('pertanyaan.tagpertanyaan', compact('data'));
    }
}
