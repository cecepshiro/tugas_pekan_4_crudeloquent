<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table='tag';
    protected $primaryKey='id';
    public $incrementing =true;
    public $timestamps=true; 
    protected $fillabe = ['id','tag','created_at','updated_at'];
    protected $guarded = [];  

    public function pertanyaan()
    {
        return $this->belongsToMany('App\Pertanyaan', 'katalog_pertanyaan');
    }
    

}
